#!/usr/bin/env bash

set -e

log () {
  echo "[$(date --rfc-3339=seconds)] ${1}"
}

log "Started"

if [[ $EUID -ne 0 ]]; then
  log "This script must be run as root"
  exit 1
fi

ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/.."
CACHE=${ROOT_DIR}/.cache
IMG_DIR="${ROOT_DIR}/img"
CREDS_FILE="${CACHE}/dropbox"
UPLOADER="${ROOT_DIR}/deps/Dropbox-Uploader/dropbox_uploader.sh"

dependencies=(curl)

for dep in "${dependencies[@]}"
do
  if command -v "${dep}" >/dev/null 2>&1; then
    # Dependency installed
    echo -n ""
  else
    log "Package ${dep} not installed"
    exit 1
  fi
done

if [ -z "${DROPBOX_TOKEN}" ]; then
  log "Variable DROPBOX_TOKEN is not set"
  exit 1
fi

mkdir -p "${CACHE}"

log "Setting dropbox credentials"
echo "OAUTH_ACCESS_TOKEN=${DROPBOX_TOKEN}" > "${CREDS_FILE}"

FILES="${IMG_DIR}/*"

for file in ${FILES}
do
  fileName=$(basename -- "${file}")
  fileTime="${fileName%.*}"
  fileDate=$(date --iso-8601 -d "@${fileTime}")

  log "Uploading ${file}"
  bash "${UPLOADER}" -f "${CREDS_FILE}" -h upload "${file}" "/${fileDate}/${fileName}"

  log "Deleting ${file} from this device"
  rm "${file}"
done

log "Finished"
echo "=========="
