#!/usr/bin/env bash

set -e

log () {
  echo "[$(date --rfc-3339=seconds)] ${1}"
}

log "Started"

ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/.."

if [[ $EUID -ne 0 ]]; then
  log "This script must be run as root"
  exit 1
fi

CACHE=${ROOT_DIR}/.cache
IMG=${ROOT_DIR}/img
LAT=52.68460619021531
LONG=-2.419417894973726
DATE=$(date --iso-8601)
URL="https://api.sunrise-sunset.org/json"
SUNSET_TIMES="${CACHE}/sunset-${DATE}.json"
NOW=$(date "+%s")
IMG_NAME="${IMG}/${NOW}.jpg"

dependencies=(curl jq raspistill)

for dep in "${dependencies[@]}"
do
  if command -v "${dep}" >/dev/null 2>&1; then
    # Dependency installed
    echo -n ""
  else
    log "Package ${dep} not installed"
    exit 1
  fi
done

mkdir -p "${CACHE}" "${IMG}"

if [ ! -f "${SUNSET_TIMES}" ]; then
  log "Downloading sunrise/sunset times for ${DATE} and saving to ${SUNSET_TIMES}"
  curl -f "${URL}?lat=${LAT}&lng=${LONG}&date=${DATE}&formatted=0" > "${SUNSET_TIMES}"
fi

SUNRISE=$(date "+%s" -d "$(< "${SUNSET_TIMES}" jq -r '.results.civil_twilight_begin')")
SUNSET=$(date "+%s" -d "$(< "${SUNSET_TIMES}" jq -r '.results.civil_twilight_end')")

if (("${NOW}" >= "${SUNRISE}" && "${NOW}" <= "${SUNSET}")); then
  log "Taking photo - saving to ${IMG_NAME}"

  raspistill \
    -h 720 \
    -w 1280 \
    -ex auto \
    -awb auto \
    -mm matrix \
    -n \
    -o "${IMG_NAME}"
else
  log "It's dark - not taking the photo"
fi

log "Finished"
echo "=========="
